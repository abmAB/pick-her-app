var win = Ti.UI.currentWindow;

var customFont = 'GloriaHallelujah';
// 'Spicy Rice';  use the friendly-name on iOS
// if(Ti.Platform.osname=='android') {
// on Android, use the "base name" of the file (name without extension)
// customFont = 'SpicyRice-Regular';
//
// }
var roundedValue='';
var currID = '';

	var button = Ti.UI.createImageView({
	image: '',
	width: '300dp',
	height: '80dp',
	bottom: '40dp'
});

var ZappsHowTo = Ti.UI.createImageView({
	width: '310dp',
	height: '72dp',
	left: '0dp',
	top: '0dp',
	image: 'images/ZappsTitle.png',

});
win.add(ZappsHowTo);


var SliderHolder = Ti.UI.createScrollView({
	backgroundColor: '#B7EEE1',
	contentHeight: 'auto',
	contentWidth: '100%',
	top: ZappsHowTo.height,
	bottom: 0,
	layout: 'vertical'
});
win.add(SliderHolder);

var sumArray=[];

var rowArray = [];

var q_and_a_Array = [
	{ q_and_a: [L('Fråga för slider 1'), L('Svar 1 slider 1'), L('Svar 2 slider 1'), L('Svar 3 slider 1'), L('Svar 4 slider 1'), L('Svar 5 slider 1'), L('Svar 6 slider 1'), L('Svar 7 slider 1'), L('Svar 8 slider 1'), L('Svar 9 slider 1'), L('Svar 10 slider 1')] },

	{ q_and_a: [L('Fråga för slider 2'), L('Svar 1 slider 2'), L('Svar 2 slider 2'), L('Svar 3 slider 2'), L('Svar 4 slider 2'), L('Svar 5 slider 2'), L('Svar 6 slider 2'), L('Svar 7 slider 2'), L('Svar 8 slider 2'), L('Svar 9 slider 2'), L('Svar 10 slider 2')]},

	{ q_and_a: [L('Fråga för slider 3'), L('Svar 1 slider 3'), L('Svar 2 slider 3'), L('Svar 3 slider 3'), L('Svar 4 slider 3'), L('Svar 5 slider 3'), L('Svar 6 slider 3'), L('Svar 7 slider 3'), L('Svar 8 slider 3'), L('Svar 9 slider 3'), L('Svar 10 slider 3')]},

	{ q_and_a: [L('Fråga för slider 4'), L('Svar 1 slider 4'), L('Svar 2 slider 4'), L('Svar 3 slider 4'), L('Svar 4 slider 4'), L('Svar 5 slider 4'), L('Svar 6 slider 4'), L('Svar 7 slider 4'), L('Svar 8 slider 4'), L('Svar 9 slider 4'), L('Svar 10 slider 4')]},

	{ q_and_a: [L('Fråga för slider 5'), L('Svar 1 slider 5'), L('Svar 2 slider 5'), L('Svar 3 slider 5'), L('Svar 4 slider 5'), L('Svar 5 slider 5'), L('Svar 6 slider 5'), L('Svar 7 slider 5'), L('Svar 8 slider 5'), L('Svar 9 slider 5'), L('Svar 10 slider 5')]},

	{ q_and_a: [L('Fråga för slider 6'), L('Svar 1 slider 6'), L('Svar 2 slider 6'), L('Svar 3 slider 6'), L('Svar 4 slider 6'), L('Svar 5 slider 6'), L('Svar 6 slider 6'), L('Svar 7 slider 6'), L('Svar 8 slider 6'), L('Svar 9 slider 6'), L('Svar 10 slider 6')]},

];



var instructions = Ti.UI.createLabel({
	text: L('instruktioner'),
	top: '10dp',
	font: {
		fontSize: '14sp',
		fontFamily: customFont,
		fontWeight: 'bold'
	}
});
SliderHolder.add(instructions);

for (var i = 0; i < 6; i++) {


	rowArray[i] = Ti.UI.createView({
		top: 0,
		height: Ti.UI.SIZE
	});

	rowArray[i].commentFieldView = Ti.UI.createImageView({
		top: '20dp',
		height: '96',
		width: '310dp',
		backgroundColor:'transparent',
		image : 'images/commentField.png'
	});
	rowArray[i].add(rowArray[i].commentFieldView);
	
	
	


	rowArray[i].theSlider = Titanium.UI.createSlider({
		top: '99dp',
		min: 0,
		max: 10,
		width: '300dp',
		value: 0,
		thumbImage: 'images/zappwithArrow.png',
		rightTrackImage: 'rightTrack.png',
		leftTrackImage: 'leftTrack.png',
		arrayID: i

	});
	rowArray[i].add(rowArray[i].theSlider);



	rowArray[i].questionLbl = Ti.UI.createLabel({
		text: q_and_a_Array[i].q_and_a[0],//'hur snygg är du?',
		top: '0dp',
		bottom: '4dp',
		left: '20dp',
		right: '70dp',
		font: {
			fontFamily: customFont,
			fontWeight: 'bold'
		},
	});
	rowArray[i].commentFieldView.add(rowArray[i].questionLbl);
	
	

	rowArray[i].numberLbl = Ti.UI.createLabel({
		text: 0,
		top: '0dp',
		right: '24dp',
		font: {
			fontSize: '64sp',
			fontFamily: customFont,
			fontWeight: 'bold'
		},
	});
	rowArray[i].add(rowArray[i].numberLbl);
	
	
	
		rowArray[i].theSlider.addEventListener('change', function(e) {
		currID = e.source.arrayID;
		roundedValue = Math.round(e.value);
		
		rowArray[currID].numberLbl.text = roundedValue;
		rowArray[currID].questionLbl.text = q_and_a_Array[currID].q_and_a[roundedValue];


	});

	SliderHolder.add(rowArray[i]);
};


function valAvKnappBild (){
	
	if (instructions.text== 'Gör en uppskattning på en skala 0-10...'){
		
   button.image='images/calculateSV.png';
   

SliderHolder.add(button);

	};
	
	if (instructions.text== 'Answer the questions on a scale 0-10...'){
		
		button.image= 'images/calculate.png' ;

SliderHolder.add(button);
		
	};
};


valAvKnappBild();





button.addEventListener('click', function() {
	
		
var	sumArray = [];
	
	
var	slider2Value= rowArray[1].theSlider.value;
var slider3Value= rowArray[2].theSlider.value;
var	slider4Value= rowArray[3].theSlider.value;
var slider5Value=rowArray[4].theSlider.value;
var slider6Value= rowArray[5].theSlider.value;


	

		sumArray.push(rowArray[0].theSlider.value);
		
var xxx='';
		
		
		
		
		
		
		
//************************************************************************************************
		
  if(rowArray[1].theSlider.value >9 && rowArray[1].theSlider.value <=10){
			
			slider2Value= 2;
			
			sumArray.push(slider2Value);
			
	//sumArray.push(rowArray[1].theSlider.value-9);
			}
		
		
		   else if(rowArray[1].theSlider.value >8  && rowArray[1].theSlider.value <= 9){ 
		   	
		   	slider2Value= 4;
		   	
		   	sumArray.push(slider2Value);
		   	
		   	
             //sumArray.push(rowArray[1].theSlider.value -5);
             }
			
			
			else if(rowArray[1].theSlider.value >7  && rowArray[1].theSlider.value <= 8){ 
				
				slider2Value=6;
				
				sumArray.push(slider2Value);
			
          //sumArray.push(rowArray[1].theSlider.value -1);
          
          }
			
			
			
		else if(rowArray[1].theSlider.value >6  && rowArray[1].theSlider.value <= 7){ 
				
				
				slider2Value= 10;
				sumArray.push(slider2Value);
			
		 //sumArray.push(rowArray[1].theSlider.value + 1);
		 
		 }
			
			
			
			
			else if(rowArray[1].theSlider.value >5  && rowArray[1].theSlider.value <= 6){ 
				
				slider2Value= 19;
				
				sumArray.push(slider2Value);
			
		//sumArray.push(rowArray[1].theSlider.value + 4);
		}
			
			
			
			
			else if(rowArray[1].theSlider.value >4  && rowArray[1].theSlider.value <= 5){
				
				slider2Value=30;
				
				sumArray.push(slider2Value); 
			
			//sumArray.push(rowArray[1].theSlider.value+10);
			}
			
			
			
			
			else if(rowArray[1].theSlider.value >3  && rowArray[1].theSlider.value <= 4){ 
				
				slider2Value= 40;
				
				sumArray.push(slider2Value);
			
			//sumArray.push(rowArray[1].theSlider.value +15);
			}
			
			
			
			else if(rowArray[1].theSlider.value >2  && rowArray[1].theSlider.value <= 3){ 
				
				slider2Value= 50;
				
				sumArray.push(slider2Value);
			
			//sumArray.push(rowArray[1].theSlider.value +20);
			}
			
			
			
			else if(rowArray[1].theSlider.value >1  && rowArray[1].theSlider.value <= 2){ 
				
				slider2Value= 60;
				
				sumArray.push(slider2Value);
			
			//sumArray.push(rowArray[1].theSlider.value +40);
			}
			
			
			
			
			else if(rowArray[1].theSlider.value >0.1  && rowArray[1].theSlider.value <= 1){ 
				
				slider2Value= 70;
				
				sumArray.push(slider2Value);
			
			 // sumArray.push(rowArray[1].theSlider.value +40);
			 }
		
		
		
	//***************************************************************************************************
		
		
		//sumArray.push(slider3Value);
		
		
		
			if(rowArray[2].theSlider.value >9 && rowArray[2].theSlider.value <=10){
			
			slider3Value= 40;
			
		  //sumArray.push(slider4Value);
		  }
		
		
		else if(rowArray[2].theSlider.value >8  && rowArray[2].theSlider.value <= 9){ 
			
			slider3Value= 35;
		}


else if(rowArray[2].theSlider.value >7  && rowArray[2].theSlider.value <= 8){ 
			
			slider3Value= 30;
		}
		
		
			
			else if(rowArray[2].theSlider.value >6  && rowArray[2].theSlider.value <= 7){ 
			
			slider3Value= 25;
}

else if(rowArray[2].theSlider.value >5  && rowArray[2].theSlider.value <= 6){ 
			
			slider3Value= 15;
			
		}
			
			else if(rowArray[2].theSlider.value >4  && rowArray[2].theSlider.value <= 5){ 
			
			slider3Value= 10;
			}
			
			else if(rowArray[2].theSlider.value >3  && rowArray[2].theSlider.value <= 4){ 
			
			slider3Value= 5;
			}
			
			
			else if(rowArray[2].theSlider.value >2  && rowArray[2].theSlider.value <= 3){ 
			
			slider3Value= 2;
			}
			
			
			
			else if(rowArray[2].theSlider.value >1  && rowArray[2].theSlider.value <= 2){ 
			
			slider3Value= 1;
			}
			
			else if(rowArray[2].theSlider.value >0.1  && rowArray[2].theSlider.value <= 1){ 
			
			slider3Value= 1;
			}








sumArray.push(slider3Value);
	//***************************************************************************************************	
		
		if(rowArray[3].theSlider.value >9 && rowArray[3].theSlider.value <=10){
			
			slider4Value= 1;
			
		  //sumArray.push(slider4Value);
		  }
		
		
		else if(rowArray[3].theSlider.value >8  && rowArray[3].theSlider.value <= 9){ 
			
			slider4Value= 2;
			
			 //sumArray.push(slider4Value);
			
			//sumArray.push(rowArray[3].theSlider.value -8.5);
			}
			
			
		else if(rowArray[3].theSlider.value >7  && rowArray[3].theSlider.value <= 8){ 
				
				slider4Value= 4;
				
				// sumArray.push(slider4Value);
			
			//sumArray.push(rowArray[3].theSlider.value -7);
			}
			
			
			
			else if(rowArray[3].theSlider.value >6  && rowArray[3].theSlider.value <= 7){ 
				
				slider4Value= 7;
				
				// sumArray.push(slider4Value);
			
			// sumArray.push(rowArray[3].theSlider.value -5);
			}
			
			
			
			
		else if(rowArray[3].theSlider.value >5  && rowArray[3].theSlider.value <= 6){
				
				 slider4Value= 11;
				 
				  //sumArray.push(slider4Value);
			
			//sumArray.push(rowArray[3].theSlider.value-3);
			}
			
			
			
			
		else if(rowArray[3].theSlider.value >4  && rowArray[3].theSlider.value <= 5){ 
				
				slider4Value= 15;
				
				// sumArray.push(slider4Value);
			
			//sumArray.push(rowArray[3].theSlider.value);
			}
			
			
			
			
			else if(rowArray[3].theSlider.value >3  && rowArray[3].theSlider.value <= 4){ 
				
				slider4Value= 18;
			
			//sumArray.push(rowArray[3].theSlider.value +4.5);
			}
			
			
			
			else if(rowArray[3].theSlider.value >2  && rowArray[3].theSlider.value <= 3){
				
				slider4Value= 22; 
			
			//sumArray.push(rowArray[3].theSlider.value +7);
			}
			
			
			
			else if(rowArray[3].theSlider.value >1  && rowArray[3].theSlider.value <= 2){ 
				
				slider4Value= 27;
			
			//sumArray.push(rowArray[3].theSlider.value +10);
			}
			
			
			
			 else if(rowArray[3].theSlider.value >0.1  && rowArray[3].theSlider.value <= 1){
				
				slider4Value= 30; 
			
			//sumArray.push(rowArray[3].theSlider.value +13);
			}
		
		
		 sumArray.push(slider4Value);
		
		
		
		
		
		
//**********************************************************************************************		
	
		if(rowArray[4].theSlider.value >9 && rowArray[4].theSlider.value <=10){
			
			slider5Value= 10;
			
			
		  //sumArray.push(rowArray[4].theSlider.value-6);
		  }
		
		
		 else if(rowArray[4].theSlider.value >8  && rowArray[4].theSlider.value <= 9){
		 	
		 	slider5Value= 14; 
			
			//sumArray.push(rowArray[4].theSlider.value -3);
			}
			
			
			 else if(rowArray[4].theSlider.value >7  && rowArray[4].theSlider.value <= 8){ 
			 	
			 	slider5Value = 20;
			
			//sumArray.push(rowArray[4].theSlider.value+1);
			}
			
			
			
			else if(rowArray[4].theSlider.value >6  && rowArray[4].theSlider.value <= 7){
			 	
			 	slider5Value= 14;
			
			//sumArray.push(rowArray[4].theSlider.value);
			}
			
			
			
			
			else if(rowArray[4].theSlider.value >5  && rowArray[4].theSlider.value < 6){
			 	
			 	slider5Value= 10;
			
			// sumArray.push(rowArray[4].theSlider.value);
			}
			
			
			
			
			 else if(rowArray[4].theSlider.value >4  && rowArray[4].theSlider.value <=5){ 
			 	
			 	slider5Value= 7;
			
			//sumArray.push(rowArray[4].theSlider.value);
			}
			
			
			
			
			
			 else if(rowArray[4].theSlider.value >3  && rowArray[4].theSlider.value <= 4){
			 	
			 	slider5Value= 4;
			
			//sumArray.push(rowArray[4].theSlider.value);
			}
			
			
			
			
			
			 else if(rowArray[4].theSlider.value >2  && rowArray[4].theSlider.value <= 3){ 
			 	
			 	slider5Value= 3;
			
			//sumArray.push(rowArray[4].theSlider.value);
			}
			
			
			
			
			 else if(rowArray[4].theSlider.value >1  && rowArray[4].theSlider.value <=2){ 
			 	
			 	slider5Value= 2;
			
			//sumArray.push(rowArray[4].theSlider.value);
			}
			
			
			
			
			 else if(rowArray[4].theSlider.value >0.1  && rowArray[4].theSlider.value <=1){ 
			 	
			 	slider5Value= 1;
			
			//sumArray.push(rowArray[4].theSlider.value);
			}
			
			sumArray.push(slider5Value);
			
			
			//Ti.API.info(slider5Value);
//********************************************************************************************			
		
		if(rowArray[5].theSlider.value >9 && rowArray[5].theSlider.value <=10){
			
		  sumArray.push(rowArray[5].theSlider.value -9);}
		
		
		else if(rowArray[5].theSlider.value >8  && rowArray[5].theSlider.value <= 9){ 
			
			sumArray.push(rowArray[5].theSlider.value -4);}
			
	
			else if(rowArray[5].theSlider.value >7  && rowArray[5].theSlider.value <= 8){ 
			
			sumArray.push(rowArray[5].theSlider.value =1);}
			
			else if(rowArray[5].theSlider.value >6  && rowArray[5].theSlider.value <= 7){ 
			
			sumArray.push(rowArray[5].theSlider.value);}
			
			else if(rowArray[5].theSlider.value >5 && rowArray[5].theSlider.value <=6){ 
			
			sumArray.push(rowArray[5].theSlider.value +10);}
			
			else if(rowArray[5].theSlider.value >4  && rowArray[5].theSlider.value <= 5){ 
			
			sumArray.push(rowArray[5].theSlider.value +22);}
			
			else if(rowArray[5].theSlider.value >3  && rowArray[5].theSlider.value <= 4){ 
			
			sumArray.push(rowArray[5].theSlider.value +35);}
			
			else if(rowArray[5].theSlider.value >2  && rowArray[5].theSlider.value <= 3){ 
			
			sumArray.push(rowArray[5].theSlider.value +50);}
			
			else if(rowArray[5].theSlider.value >1  && rowArray[5].theSlider.value <= 2){ 
			
			sumArray.push(rowArray[5].theSlider.value +30);}
			
			else if(rowArray[5].theSlider.value >0.1  && rowArray[5].theSlider.value <= 1){ 
			
			sumArray.push(rowArray[5].theSlider.value +20);}

	
 // if(slider2Value==4 && slider3Value< 0 && slider4Value<0 && slider5Value<0 && slider6Value<0 && rowArray[0].theSliderValue< 0){
// 	
	// xxx=0;
// 	
// 	
// }


	var sum2 = 0;
	
	
	for(var a = 0; a < sumArray.length; a++){
		
		sum2 = sum2 + sumArray[a];
	}
	
	 Titanium.API.info(sum2);
	 
	 if 
	 (sum2<0.1){
	 	xxx=100;
	 }
	
    if (rowArray[1].theSlider.value >= 7.5  && rowArray[1].theSlider.value <=10 && rowArray[5].theSlider.value >= 7.5 ){
		
		sum2/20;
		
	}
	
	
	
	

	if (rowArray[5].theSlider.value >= 6.5 && rowArray[5].theSlider.value < 7 || rowArray[2].theSlider.value <= 4.5 && rowArray[2].theSlider.value >3.5 ) {
		
		sum2=sum2*0.7;
		
		}
		
			if (rowArray[0].theSlider.value<=3.5 ){
			
			sum2= sum2* 0.7;
		}
		if (rowArray[2].theSlider.value<=3.5 ){
			
			sum2= sum2* 0.4;
		}
		
		if (rowArray[5].theSlider.value >= 7 && rowArray[5].theSlider.value < 8.5){
		
		sum2=sum2*0.45;
		
	}
	
	if (rowArray[1].theSlider.value >= 7.5){
		
		sum2=sum2*0.45;
	} 
		
	if (rowArray[1].theSlider.value < 3){
		
		sum2=sum2*1.4;
	} 
	 sum2=sum2/5;
	 
	
	
	roundedValue2 = Math.round(sum2);
	
	if (roundedValue2< 1){ 
		roundedValue2 = 1;
		};

	var calculateWin = Ti.UI.createWindow({
		backgroundColor: 'pink',
		url: 'calculateWin.js',
		sum2: roundedValue2,
		lazy: xxx,
		rowArray5: rowArray[5].theSlider.value
	});

	
	 calculateWin.open();
});