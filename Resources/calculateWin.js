var win = Ti.UI.currentWindow;

var customFont = 'GloriaHallelujah';
// use the friendly-name on iOS
// if(Ti.Platform.osname=='android') {
// on Android, use the "base name" of the file (name without extension)
// customFont = 'SpicyRice-Regular';
//
// }
var closeButton = Ti.UI.createLabel({

	color : '#FFFFFF',
	bottom : '15dp',
	text : L('stäng'),
	font : {
		fontSize : '22sp',
		fontFamily : customFont
	},
	fontWeight : 'bold',
});

closeButton.addEventListener('click', function() {

	win.close();
});

win.add(closeButton);

var procent = Ti.UI.createImageView({
	layout : 'horizontal',
	top : '40dp',
	width : '320dp',
	height : 'auto',
	image : 'images/procentComment.png'

});

win.add(procent);

var procentLabelBeginning = Ti.UI.createLabel({
	top : '15dp',
	text : L('You have'),
	left : '10dp',
	font : {
		fontSize : '14sp',
		fontFamily : customFont
	},
});

var procentLabelEnd = Ti.UI.createLabel({
	top : '15dp',
	text : L('hook up'),
	//right:'10dp',
	font : {
		fontSize : '14sp',
		fontFamily : customFont
	},
});

var procentLabel = Ti.UI.createLabel({
	top : '0dp',
	text :' '+ win.sum2+ '% ',
	//left:'30dp',
	font : {
		fontSize : '26sp',
		fontFamily : customFont
	},
});

procent.add(procentLabelBeginning);
procent.add(procentLabel);
procent.add(procentLabelEnd);

var advice = Ti.UI.createImageView({
	left : '3dp',
	top : '170dp',
	width : '310dp',
	height : '270dp',
	image : 'images/Advice.png'
	

});

win.add(advice);


var scrollView = Ti.UI.createScrollView({
	backgroundColor : 'transparent',
	height : '170dp',
	contentHeight : 'auto',
	contentWidth : '100%',
	width : '270dp',
	top : '80dp',
	left : '28dp',
	 bottom : 0,

});

advice.add(scrollView);

var adviceText = Ti.UI.createLabel({
	text : L('råd'),
	top : '5dp',
	width : 'auto',
	left : '8dp',
	right : '20dp',
	//textAlign : 'center',
	font : {
		fontSize : '16sp',
		fontFamily : customFont
	},
});

scrollView.add(adviceText);

var adviceButton = Ti.UI.createLabel({

	color : '#FFFFFF',
	top : '160dp',
	text : L('tryck här'),
	font : {
		fontSize : '22sp',
		fontFamily : customFont
	},
	fontWeight : 'bold',
});

var adviceButton2 = Ti.UI.createLabel({

	color : '#FFFFFF',
	height : '20dp',
	width : '180dp',
	top : '190dp',
	left : '100dp',
	text : L('tryck här igen'),
	font : {
		fontSize : '12sp',
		fontFamily : customFont
	},
	fontWeight : 'bold',
});

win.add(adviceButton2);
win.add(adviceButton);

var adviceBelow10 = [];

adviceButton.addEventListener('click', function(e) {
	
	myAdvice();
	
	scrollView.scrollTo(0,0);


});




// function randomBelow5Procent(min, max) {
	// return Math.floor(Math.random() * (max - min + 1) + min);
//
//};
// 
// function random515(min,max){
	// return Math.floor(Math.random() * (max - min + 1 ) + min);
// };

function myAdvice() {
	
	if (win.lazy == 100) {
		
		adviceText.text=L('om man inte flyttat sliders');
		Ti.API.info('xxx körs');
		return;
	};
	
	if (win.rowArray5 >= 8.5){
		
		adviceText.text= L('Jag har bara ett råd. Hitta nån som är lika packad som du eller åk hem!!!');
		procentLabel.text= '1 %';
		
		return;
	};
	
	
	
	
	if (win.sum2<=5){
 
		var adviceBelow5 = [
			L('Det är nog kört!'), 
			L('Wow, det var inte bra!'), 
			L('Hur tänkte du nu? Jag är ingen trollkarl...'), 
			L('Med den procenten hoppas jag att du har cash!'),
			L('Är hon blind och döv så har du nog en chans!'), 
			L('Hur ska jag kunna hjälpa dig om du inte hjälper dig själv!'),
			 
		
		];
		
		//adviceText.text = (adviceBelow10[randomBelow10Procent(1, adviceBelow10.length) - 1]);			
		currentTextCount = Ti.App.Properties.getInt("adviceBelow5", 0);
		adviceText.text = adviceBelow5[currentTextCount];
		currentTextCount++;
		if(currentTextCount ==adviceBelow5.length)
		//Ti.APP.info(adviceBelow.lenght);
		{
			currentTextCount = 0;
		}
		Ti.App.Properties.setInt("adviceBelow5", currentTextCount);

	};


//*****************************************************************
	
	
	


	if (win.sum2 >5 && win.sum2 <= 100){
	 
	 	var advice540= [
	 	
	 	L('1'),
	 	L('1a'),
	 	L('1b'),
	 	L('1c'),
	 	L('2'),
	 	L('3'),
	 	L('4'),
	 	L('5'),
	 	L('6'),
	 	L('7'),
		L('8'),
		L('9'),
		L('10'),
		L('11'),
		L('12'),
		L('13'),
		L('14'),
		L('15'),
		L('16'),
	 	L('17'),
	 	L('18'),
	 	L('19'),
	 	L('20'),
	 	L('21'),
	 	L('22'),
		L('23'),
		L('24'),
		L('25'),
		L('26'),
		L('27'),
		L('28'),
		L('29'),
		L('30'),
		L('31'),
		L('32'),
		L('33'),
		L('34'),
		L('35'),
		L('36'),
		L('37'),
		L('38'),
		L('39'),
		L('40'),
		L('41'),
		L('42'),
		L('43'),
		L('44'),
		L('45'),
		L('46'),
		L('47'),
		L('48'),
		L('49'),
		L('50'),

	 	];
	 	
	 	currentTextCount4= Ti.App.Properties.getInt('advice540', 0);
	 	adviceText.text= advice540[currentTextCount4];
	 	currentTextCount4++;
	 	
	 	 if(currentTextCount4 == advice540.length)
		//Ti.API.info(advice515.lenght);
		{
			currentTextCount4 = 0;
		}
	 	Ti.App.Properties.setInt('advice540',currentTextCount4);
	 	
 		//adviceText.text= (advice515[random515(1,advice515.length)-1]);	
	};
	
	
	
	if (win.sum2 > 100){
		
		adviceText.text= 'Not possible!';
	};

};


myAdvice();

/* ------------ LOAD RANDOM IMAGE SET ON STARTUP ------------ */
// function loadStartupArray() {
// 
	// var from = 0;
// 
	// var randomNumberInArray = Math.floor(Math.random() * (lengthOfArray - from) + from);
// 
	// imgLoaderTableView(randomNumberInArray);
// 
	// tableView.selectRow(randomNumberInArray);
// 
	// showCityLabel();
// 
// };


